# Currículo - http://www.sitedoph.com.br/sobre-o-ph

Meu currículo feito com LaTeX. 

# Para compilar com o Docker:

    $ docker run --rm registry.gitlab.com/palerique/docker-xelatex:latest xelatex cv-completo-colorido.tex xelatex cv-completo-peb.tex xelatex cv-completo-colorido-english.tex xelatex cv-completo-peb-english.tex
    $ docker run --rm -v $(pwd):/data registry.gitlab.com/palerique/docker-xelatex:latest make

# Crédito:

Feito com base no exemplo disponível em: https://github.com/afriggeri/cv
