######################
#      Makefile      #
######################

pdf: 
	xelatex cv-completo-colorido.tex
	xelatex cv-completo-peb.tex
	xelatex cv-completo-colorido-english.tex
	xelatex cv-completo-peb-english.tex

text: html
	html2text -width 100 -style pretty cv-completo-colorido/cv-completo-colorido.html | sed -n '/./,$$p' | head -n-2 >cv-completo-colorido.txt
	html2text -width 100 -style pretty cv-completo-peb/cv-completo-peb.html | sed -n '/./,$$p' | head -n-2 >cv-completo-peb.txt
	html2text -width 100 -style pretty cv-completo-colorido-english/cv-completo-colorido-english.html | sed -n '/./,$$p' | head -n-2 >cv-completo-colorido-english.txt
	html2text -width 100 -style pretty cv-completo-peb-english/cv-completo-peb-english.html | sed -n '/./,$$p' | head -n-2 >cv-completo-peb-english.txt

html:
	@#latex2html -split +0 -info "" -no_navigation cv-completo-colorido
	htlatex cv-completo-colorido
	@#latex2html -split +0 -info "" -no_navigation cv-completo-peb
	htlatex cv-completo-peb
	@#latex2html -split +0 -info "" -no_navigation cv-completo-colorido-english
	htlatex cv-completo-colorido-english
	@#latex2html -split +0 -info "" -no_navigation cv-completo-peb-english
	htlatex cv-completo-peb-english

view:
	while inotifywait --event modify,move_self,close_write cv-completo-peb.tex; \
		do xelatex -halt-on-error cv-completo-peb &&   xelatex -halt-on-error \
		cv-completo-peb; done

clean:
	rm -f cv-completo-colorido.{ps,pdf,log,aux,out,dvi,bbl,blg,snm,toc,nav}
	rm -f cv-completo-peb.{ps,pdf,log,aux,out,dvi,bbl,blg,snm,toc,nav}
	rm -f cv-completo-colorido-english.{ps,pdf,log,aux,out,dvi,bbl,blg,snm,toc,nav}
	rm -f cv-completo-peb-english.{ps,pdf,log,aux,out,dvi,bbl,blg,snm,toc,nav}
